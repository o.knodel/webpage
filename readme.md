Working version of LEAPS WG3 landing page.

# Local builds
For local builds, you will need to install [Jekyll](https://jekyllrb.com/).
```
~ $ gem install bundler jekyll
~ $ git clone <this repo>
~ $ cd leaps-wg3-webpage
~/leaps-wg3-webpage $ bundle install
~/leaps-wg3-webpage $ bundle exec jekyll serve
```
Now browse to http://localhost:4000 and voilà.
