---
layout: post
title: PaN science interest group session at the international data week 2023 in Salzburg
category: workshop
pic: https://www.rd-alliance.org/sites/default/files/idw2023%20image.jpg
link: https://www.rd-alliance.org/plenaries/international-data-week-2023-salzburg/tools-and-methods-working-towards-fair-facility
excerpt: 'Our session on "tools and methods working towards the FAIR facility" has been accepted. We look forward to discussing the FAIR toolkit developed in the frame of ExPaNDS and PaNOSC, sharing it with the wider RDA community and comparing with similar efforts in facilities around the world. We would also like to discuss how we will further its adoption and continue to develop it within future programmes.'
---
## Meeting agenda
### Approaches to Implementing FAIR Data in Facilities Internationally – 35 mins
1. A FAIR data toolkit for European PaN RIs -  Brian Matthews (STFC) (contribution from Andy Götz - ESRF) - 10 mins
2. FAIR principles adoption in US laboratories - Line Pouchard (BHNL), Jon Taylor (SNS) - 15 mins
3. RDM strategies for photon and neutron facilities in Germany - Markus Kubin (HZB) - 10 mins

### Tools to support FAIR Data in Facilities - 25 mins
1. Data Management Plans within facilities processes - Heike Görzig (HZB)
2. FAIR vocabularies - Alejandra Gonzalez-Beltran (STFC)
3. PIDs for Scientific Data: Applications in Synchrotron and FEL Facilities - Andrei Vukolov (Elettra)

### Future activities of PaN community on FAIR practices - 15 mins
1. Building on the ExPaNDS, PaNOSC and EOSC-Future projects in OSCARS - Sophie Servan (DESY)
2. The work of LEAPS Working Group 3 and developing an international MoU on RDM -  Majid Ounsy (Soleil)

### Discussion on Future IG activities - 15 mins

## Venue
This session is part of the International Data Week 2023 that will happen in Salzburg from 23 to 26 October 2023. International Data Week brings together a global community of data scientists and data stewards; researchers from all domains; data, interoperability and informatics experts from all fields (including geoinformatics, bioinformatics, cheminformatics etc); industry leaders, entrepreneurs and policymakers.

Early-bird registration [is open here](https://internationaldataweek.org/idw2023/registration/) until September 15, 2023.