---
layout: post
title: Two joint NFFA-Europe workshops on open data, microscopy and analytical large-scale facilities
category: workshop
pic: https://www.nffa.eu/media/382578/new_pagina-web-nffa-workshops-events.jpg?anchor=center&mode=resize&width=1000
link: https://www.nffa.eu/news/events/two-joint-nffa-europe-workshops-on-microscopy-open-data-and-analytical-large-scale-facilities/
excerpt: 'NFFA-Europe is glad to announce two upcoming workshops which will explore some highly relevant issues in the world of nanoscience and nanotechnology. The two half-day events will take place at the MLZ (Heinz Maier-Leibnitz Zentrum) premises in Garching, Munich (Germany) on September 26th.'
---
The first workshop “**FAIR and Open Data in NFFA-Europe Pilot and
Beyond**”, scheduled in the morning (9:00-13:00), will be focused on
Open Data practices and FAIR principles and their relevance for
research infrastructure activities and, more in general, for the
advancement of science.

The first four speakers will present the approaches adopted by the
**NFFA-Europe Pilot project**, sharing insights and successful
stories. Special attention will be devoted to the solutions and tools
devised and implemented during the project’s research journey. In the
second part of the workshop, other European research projects will
take the stage: **FAIRMAT**, **NFDI-MatWerk**, **MARVEL** and **GO
FAIR**. Speakers will discuss practical use cases and provide further
valuable insights.

The second workshop “**Bridging Microscopy and Analytical Large-Scale
Facilities**”, scheduled in the afternoon (14:00-17:50), will switch
the focus to another aspect of paramount importance in nanoscience
research: how to seamlessly combine cutting-edge experiments at
analytical large-scale facilities with complementary lab-based
microscopy.

The workshop will explore the opportunity to collect data and
information at the nanoscale **using different imaging modalities**,
an issue which is highly attractive for interdisciplinary nanoscience
communities. The eight speakers will present relevant results and
discuss the way forward to **establish a user platform** which will
allow to collect correlative structural and chemical information of
nanoscale objects.

The workshops can be attended either in person or remotely.

## Tentative agenda: FAIR and open data in NFFA-Europe pilot and beyond

### FIRST SESSION: FAIR data @ NFFA-Europe Pilot

|---|---|
| 09:00 - 09:10 | Welcome and Introduction |
| 09:10 - 09:30 | General approach to FAIRification in NFFA-Europe Pilot |
| 09:30 - 09:50 | Open data and open publications in H2020 |
| 09:50 - 10:10 | Successful stories in NFFA-Europe Pilot and beyond |
| 10:10 - 10:30 | Towards FAIR data in practice: the Scanning Tunneling Microscopy use case |
| 10:30 - 11:00 | Coffee break |

### SECOND SESSION: Insights from other communities

|---|---|
| 11:00 - 11:20 | FAIRMAT Multi-Dimensional Photoemission Spectroscopy: a concept for FAIR photoemission data |
| 11:20 - 11:40 | NFDI-MatWerk Digital Materials Environment – An Architecture and Tools based on FAIR Digital Objects for the NFDI-MatWerk |
| 11:40 - 12:00 | NCCR MARVEL and MaX CoE More than open data: towards a FAIR data and simulation infrastructure with AiiDA and Materials Cloud |
| 12:00 - 12:30 | GO FAIR Approaching FAIR data management in nanotechnologies and semiconductor cleanrooms, the ENL experience |
| 12:30 - 12:50 | Open discussion |
| 12:50 - 13:00 | Closing remarks |


## Tentative agenda: Bridging microscopy and analytical large-scale facilities

### FIRST SESSION

|---|---|
| 14:00 - 14:30 | Colloidal doped-quantum dots for sensing and photocatalysis: from chemical design to advanced physicochemical features |
| 14:30 - 14:50 | Elucidating the Structural Pathways for Lipid Membrane Solubilization using Electrons, Neutrons and X-rays |
| 14:50 - 15:10 | Combination of small-angle neutron scattering and electron microscopy opens new vistas in ultrastructural studies of photosynthetic membranes |
| 15:10 - 15:40 | Superchaotropic nano-ion binding as a gelation motif in cellulose ether solutions |
| 15:40 - 16:10 | Coffee break |

### SECOND SESSION

|---|---|
| 16:10 - 16:40 | Revealing chemical heterogeneity in the surface layer of a tooling alloy by collaborative imaging using synchrotron X-ray techniques |
| 16:40 - 17:00 | Correlative X-ray imaging at ESRF beamline ID21: challenges, developments and recent applications |
| 17:00 - 17:20 | Correlative imaging of Materials: Bridging 2D and 3D |
| 17:20 - 17:40 | TBD |
| 17:40 - 17:50 | Closing remarks |

When: 2023-09-26

Location: Garching/Munich (Germany)

Organizer: NFFA-Europe
