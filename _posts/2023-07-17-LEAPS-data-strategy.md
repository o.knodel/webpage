---
layout: post
title: LEAPS data strategy white paper published 
category: publication
pic: https://media.springernature.com/full/springer-static/image/art%3A10.1140%2Fepjp%2Fs13360-023-04189-6/MediaObjects/13360_2023_4189_Fig2_HTML.png?as=webp
link: https://doi.org/10.1140/epjp/s13360-023-04189-6
excerpt: 'Our LEAPS data strategy paper has been accepted! It presents the strategy of our facilities to address the data challenges of the current and future photon sources in Europe.'
---
## Abstract
The continuous evolution of photon sources and their instrumentation enables more and new scientific endeavors at ever increasing pace. This technological evolution is accompanied by an exponential growth of data volumes of increasing complexity, which must be addressed by maximizing efficiency of scientific experiments and automation of workflows covering the entire data lifecycle, aiming to reduce data volumes while producing FAIR and open data of highest reliability. This papers briefly outlines the strategy of the league of European accelerator-based photon sources user facilities to achieve these goals collaboratively in an efficient and sustainable way which will ultimately lead to an increase in the number of publications.