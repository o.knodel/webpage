# Contributing to the LEAPS-IT webpage
Want to make changes to the [LEAPS-IT webpage](https://leaps-wg3.desy.de/)? You are in the right repository.

## Pre-requisite
* __Login__ with the "Helmholtz AAI" method, using your home institute’s credentials.  
_If that doesn't work, or if you don't find your home institute, please refer to [the documentation](https://hifis.net/doc/helmholtz-aai/list-of-connected-organisations/)._
* __Install jekyll__ on your machine following the instructions in [readme.md](/readme.md). You will need that to check your changes are rendering well before pushing them, as we do not offer a preview tool yet.

## Adding or editing a [member](https://leaps-wg3.desy.de/members.html)
* In [`_data/members.yml`](_data/members.yml), add or modify an entry, following the schema:
```
- surname: Millar
  first-name: Paul
  facility: DESY
  pic: "assets/images/open-peep-32.png"
  orcid: 0000-0002-3957-1279
```
* For the picture, you can either:
  * skip the `pic:` row: an image will be randomly chosen from the available avatars
  * choose an avatar in [assets/images/](assets/images/) and use `pic: "assets/images/open-peep-XX.png"`
  * upload your own pic - please choose a low-res picture of a few 10s KB - in [assets/images/](assets/images/) and link it with `pic: "assets/images/your-pic.xxx"`
* Check you like the result navigating to your local server, usually at http://127.0.0.1:4000/members.html.
* Push your changes directly to `master` if you are a member, creating a MR otherwise.

## Adding a [post](https://leaps-wg3.desy.de/posts.html)
* In [`_posts`](_posts), create a new file using the convention: `YYYY-MM-DD_title.md`
* Add front matter using the following schema:
```
---
layout: post
title: Your post's title (mandatory)
category: publication / workshop / anything else (optional)
pic: link to a picture (optional)
link: link to more resources (optional)
excerpt: 'A teaser or excerpt of the content of the post' (mandatory)
---
```
* Add your content in the body of the .md file.
* Check you like the result navigating to your local server, usually at http://127.0.0.1:4000/posts.html.
* Push your changes directly to `master` if you are a member, creating a MR otherwise.

## Edit your facility-related data
If you'd like to update the data concerning your facility, be it:
- its adoption status of outcomes in the [adoption table](https://leaps-wg3.desy.de/outcomes.html#adoption-table)
- its involvement in current projects in the [projects table](https://leaps-wg3.desy.de/current-work.html#gotoprojects)
- its open data repository and access methods in the [PaN facilities repositories table](https://leaps-wg3.desy.de/open-data-resources.html#gotofacility)  

You will need to go the [`_data/facilities.yml`](_data/facilities.yml) and edit the data about your facility, following the schema:
```
## PSI ##
- short-name: PSI
  pid:
    - type: ROR
      id: 03eh3y714
  adoption: # of outcomes for the adoption table, choosing between 'adopted', 'wip' (work in progress), 'planned', 'ue' (under evaluation), 'no' (will not be adopted)
    adopted:
      - Search-API
      - OAI-PMH
      - PaNET
      - Search
    planned:
      - FAIR-IF
    not:
      - VISA
      - VINYL
      - PaN-training
  projects: # list of projects the facility participates in for the current projects table
    - ExPaNDS
    - EOSC-Future
  odr: # links to open data repository and access methods
    link: https://doi.psi.ch/
    oai-pmh-endpoint: 
      link: https://doi.psi.ch/oaipmh/oai?verb=Identify
      status: Active
      last-check: 2023-08-07
    pan-search-api:
      link: https://dacat.psi.ch/panosc-explorer/
      status: Active
      last-check: 2023-08-07
```
* Check you like the result navigating to your local server, usually at http://127.0.0.1:4000/.
* Push your changes directly to `master` if you are a member, create a MR otherwise.

## Anything else
Many things could be better in this webpage, feel free to suggest any other change.
* __In this case, even if you are a member, create a MR.__